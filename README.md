[**Java Spring template project**](https://gitlab.com/finanzas-seguras/ms-auth)

This project is based on a GitLab [Project Template.](https://docs.gitlab.com/ee/user/project/working_with_projects.html)

**CI/CD with Auto DevOps**

This template is compatible with [Auto DevOps.](https://docs.gitlab.com/ee/topics/autodevops/)

If Auto DevOps is not already enabled for this project, you can [turn it](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) on in the project settings.
